import React, { useEffect, useState } from "react";
import { getProduct } from "../admin/helper/adminapicall";
import { getProducts } from "./helper/coreapicalls";
import { useParams } from "react-router-dom";
import queryString from "query-string";
import ImageHelper from "./helper/ImageHelper";
import {
  addItemToCart,
  removeItemFromCart,
  updateCart,
  loadCart,
} from "./helper/cartHelper";
import { getQuantityFromCart } from "./helper/cartHelper";
import Menu from "./Menu";

const Product = ({addToCartButton = false,
	removeFromCartButton = false,
	setReload = (f) => f,
	reload = undefined,

}) => {


  const { productId } = useParams();
	const [products, setProducts] = useState("");
  const [product, setProduct] = useState("");
  const [name, setName] = useState("None");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("None");
  const [stock, setStock] = useState(0);
  const [sold, setSold] = useState(0);

  const [category, setCategory] = useState("");
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);

  const preload = (productId) => {
    setLoading(true);
    getProduct(productId)
      .then((data) => {
        if (data.error) {
          setError(data.error);
        } else {
          setProduct(data);
          setName(data.name);
			setPrice(data.price);
			setDescription(data.description);
			setStock(data.stock);
			setSold(data.sold);
          setCategory(data.category.name);
          setLoading(false);
        }
      })
      .catch((err) => console.log(err));
  };

  const loadAllproducts = () => {
	setLoading(true);
	return getProducts()
	.then((data) => {
	  setLoading(false);
	  if (data.error) {
		setError(data.error);
	  } else {
		setProducts(data);
	  }
	})
	.catch((err) => {
	  console.log(err);
	  setError(err);
	  setLoading(false);
	});
  }

  useEffect(() => {
    preload(productId);
	loadAllproducts();
  }, []);

  const addToCart = () => {
  	let cart = loadCart();

  	//For empty Cart
  	if (cart.length === 0) {
  		product.quantity = 1;
  		addItemToCart(product, () => { });
  		//For non-empty Cart
  	} else {
  		let checkDuplicate = cart.filter((item) => item._id === product._id);
  		//For duplicate Product
  		if (checkDuplicate.length) {
  			cart.forEach((item) => {
  				if (item._id === product._id) {
  					incQuantity();
  				}
  			});
  			//For non-duplicate Product
  		} else {
  			product.quantity = 1;
  			addItemToCart(product, () => { });
  		}
  	}
  	setReload(!reload);
  };
  const removeFromCart = () => {
  	removeItemFromCart(product._id, () => { });
  	product.quantity = "";
  	setReload(!reload);
  };

  const showaddToCartButton = (addToCartButton) => {
  	return (
  		(addToCartButton || !product.quantity) && (
  			<button className="btn btn-info btn-block rounded" onClick={addToCart}>
  				Add to Cart
  			</button>
  		)
  	);
  };

  const showremoveFromCartButton = (removeFromCartButton) => {
  	return (
  		(removeFromCartButton || product.quantity) && (
  			<button
  				className="btn btn-danger btn-block rounded"
  				onClick={removeFromCart}
  			>
  				Remove from Cart
  			</button>
  		)
  	);
  };

  //Handle product Quantity
  const incQuantity = () => {
  	let cart = loadCart();
  	cart.forEach((item) => {
  		if (item._id === product._id) {
  			item.quantity++;
  			updateCart(cart);
  			setReload(!reload);
  			//*Updates quantity in homepage cards
  			if (products) {
  				getQuantityFromCart(products);
  			}
  		}
  	});
  };

  //Handle product Quantity
  const decQuantity = () => {
  	let cart = loadCart();
  	cart.forEach((item) => {
  		if (item._id === product._id) {
  			if (item.quantity > 1) {
  				item.quantity--;
  				updateCart(cart);
  				//*Updates quantity in homepage cards
  				if (products) {
  					getQuantityFromCart(products);
  				}
  			} else {
  				removeFromCart();
  			}
  			setReload(!reload);
  		}
  	});
  };

  const showQuantityButton = () => {
  	return product.quantity ? (
  		<div className="mb-2">
  			<p className="m-1" style={{ display: "inline" }}>
  				Qty:
  			</p>
  			{product.quantity > 0 ? (
  				<button
  					onClick={decQuantity}
  					className="btn btn-light mx-2 px-2 rounded"
  					style={{ fontSize: "0.5rem" }}
  				>
  					<i className="fa fa-minus" aria-hidden="true"></i>
  				</button>
  			) : (
  					""
  				)}
  			<p style={{ display: "inline" }}>
  				<span className="badge badge-secondary">{product.quantity}</span>
  			</p>
  			<button
  				onClick={incQuantity}
  				className="btn btn-light mx-2 px-2 rounded"
  				style={{ fontSize: "0.5rem" }}
  			>
  				<i className="fa fa-plus" aria-hidden="true"></i>
  			</button>
  		</div>
  	) : (
  			""
  		);
  };

  return (
    <div>
      <Menu />

	  <div style={{ marginTop: "100px", textAlign: "center" }}>
        <h1 className="text-white mx-auto mb-5 ">Product Details</h1>
      </div>
	  <div className="col d-flex justify-content-center">
        <div
          style={{
            width: "18rem",
          }}
          className="card text-secondary m-2 "
        >
			<ImageHelper product={product} />
          <div style={{ textAlign: "center" }} className="card-body">
		  <h3 className="card-title">{name}</h3>
		  <h4 className="badge badge-dark px-3 py-2">₹ {price}</h4>
				<br></br>
				<h4 className="badge badge-secondary px-3 py-2"> {category}</h4>
				<br></br>
		
				<h6 className="card-text">Qty: {stock}</h6>
		  		<br></br>
				<h6 className="card-text">Sold: {sold}</h6>
				<hr></hr>
				<h6 className="card-text">About: {description}</h6>
		
          </div>
        </div>
      </div>

    </div>
  );
};

export default Product;
