import React from "react";
import "../styles.css";
import Base from "../core/Base";

import Menu from "./Menu";

const Contact = () => {
  return (
    <div>
      <Menu />

      <div style={{ marginTop: "100px", textAlign: "center" }}>
        <h1 className="text-white mx-auto mb-5 ">Contact Us</h1>
      </div>
      <div className="col d-flex justify-content-center">
        <div
          style={{
            width: "18rem",
          }}
          className="card text-secondary m-2 "
        >
          <div style={{ textAlign: "center" }} className="card-body">
            <br></br>
            <h5>
              Phone: <span>+91841264100</span>{" "}
            </h5>
            <br></br>
            <h5>
              Email: <span>outfitlane@pm.me</span>{" "}
            </h5>
            <br></br>
            <br></br>
            <p className="card-text">Mon-Fri (9 AM - 5PM)</p>
            <br></br>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
